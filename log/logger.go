// +build !appengine

package log

import (
	"context"

	"log"
)

func init() {
	logger = &localLogger{}
}

type localLogger struct{}

func (l *localLogger) Logf(ctx context.Context, level int, format string, args ...interface{}) {
	log.Printf(format, args...)
}
