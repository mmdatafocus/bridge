// +build appengine

package log

import (
	"context"

	"google.golang.org/appengine/log"
)

func init() {
	logger = &appEngineLogger{}
}

type appEngineLogger struct{}

func (l *appEngineLogger) Logf(ctx context.Context, level int, format string, args ...interface{}) {
	switch level {
	case 0:
		log.Debugf(ctx, format, args...)
	case 1:
		log.Infof(ctx, format, args...)
	case 2:
		log.Warningf(ctx, format, args...)
	case 3:
		log.Errorf(ctx, format, args...)
	case 4:
		log.Criticalf(ctx, format, args...)
	default:
		log.Infof(ctx, format, args...)
	}
}
